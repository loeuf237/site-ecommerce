package com.paulin.produits.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paulin.produits.entities.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long> {

}
